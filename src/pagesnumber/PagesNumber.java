package pagesnumber;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.hpsf.MarkUnsupportedException;
import org.apache.poi.hpsf.NoPropertySetStreamException;
import org.apache.poi.hpsf.PropertySetFactory;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class PagesNumber {

    public static void main(String[] args) throws IOException, InvalidFormatException, NoPropertySetStreamException, MarkUnsupportedException {                               
//        
//        FileInputStream fis = new FileInputStream(new File("C:/doc3.docx"));
//
//        File summaryFile = new File("C:/Users/Personal/Documents/doc3.docx");

        String path;       
        
        path = args[args.length-1];
        
        XWPFDocument doc =  new XWPFDocument(POIXMLDocument.openPackage(path));
        
        System.out.println(doc.getProperties().getExtendedProperties().getUnderlyingProperties().getPages());
        
    }
    
}